# Installation
> `npm install --save @types/repeating`

# Summary
This package contains type definitions for repeating ( https://github.com/sindresorhus/repeating#readme ).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/repeating

Additional Details
 * Last updated: Wed, 03 Apr 2019 00:41:12 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by Claas Ahlrichs <https://github.com/claasahl>.
